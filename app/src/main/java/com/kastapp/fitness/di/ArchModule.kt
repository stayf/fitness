package com.kastapp.fitness.di

import com.kastapp.fitness.ui.lessons.LessonsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ArchModule {
    val module = module {
        viewModel { LessonsViewModel(get()) }
    }
}
