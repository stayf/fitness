package com.kastapp.fitness.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kastapp.fitness.data.local.db.dao.LessonDao
import com.kastapp.fitness.data.model.Lesson

@Database(
    entities = [
        Lesson::class
    ],
    version = 1, exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun lessonDao(): LessonDao

    override fun clearAllTables() {
        lessonDao().deleteAll()
    }
}
