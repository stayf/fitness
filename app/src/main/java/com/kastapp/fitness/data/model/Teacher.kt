package com.kastapp.fitness.data.model

import com.google.gson.annotations.SerializedName

data class Teacher(
    @SerializedName("short_name")
    val shortName: String,
    val name: String,
    val position: String,
    val imageUrl: String
)