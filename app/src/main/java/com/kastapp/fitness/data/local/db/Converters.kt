package com.kastapp.fitness.data.local.db

import com.google.gson.Gson
import androidx.room.TypeConverter
import com.kastapp.fitness.data.model.Teacher
import com.kastapp.fitness.util.fromJson

class Converters {
    val gson = Gson()

    @TypeConverter
    fun fromTeacherInfo(teacher: Teacher?): String? {
        return if (teacher == null) null else gson.toJson(teacher);
    }

    @TypeConverter
    fun toTeacherInfo(str: String?): Teacher? {
        return if (str.isNullOrEmpty()) null else gson.fromJson(str)
    }
}