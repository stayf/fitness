package com.kastapp.fitness

import android.app.Application
import android.util.Log
import com.facebook.cache.disk.DiskCacheConfig
import com.facebook.common.util.ByteConstants
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.kastapp.fitness.di.ArchModule
import com.kastapp.fitness.di.LocalDataModule
import com.kastapp.fitness.di.NetworkModule
import com.kastapp.fitness.di.RepositoryModule
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initLogs()
        initDI()
        initFresco()
    }

    private fun initLogs() {
        Timber.plant(if (BuildConfig.DEBUG) Timber.DebugTree() else object : Timber.Tree() {
            override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                if (priority >= Log.WARN) {
                    // тут можно вызвать крашлитику
                }
            }
        })
        RxJavaPlugins.setErrorHandler { throwable -> Timber.e(throwable) }
    }

    private fun initDI() {
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(
                listOf(
                    NetworkModule.module,
                    LocalDataModule.module,
                    RepositoryModule.module,
                    ArchModule.module
                )
            )
        }
    }

    private fun initFresco() {
        val diskCacheConfig = DiskCacheConfig.newBuilder(this)
            .setMaxCacheSize(30 * ByteConstants.MB.toLong())
            .setMaxCacheSizeOnLowDiskSpace(15 * ByteConstants.MB.toLong())
            .setMaxCacheSizeOnVeryLowDiskSpace(5 * ByteConstants.MB.toLong())
            .build()
        val frescoConfig = ImagePipelineConfig.newBuilder(this)
            .setDownsampleEnabled(true)
            .setMainDiskCacheConfig(diskCacheConfig)
            .setSmallImageDiskCacheConfig(diskCacheConfig)
            .build()
        Fresco.initialize(this, frescoConfig)
    }
}
