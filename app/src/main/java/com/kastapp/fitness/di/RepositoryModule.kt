package com.kastapp.fitness.di

import com.kastapp.fitness.data.repository.LessonRepository
import org.koin.dsl.module

object RepositoryModule {
    val module = module {
        single { LessonRepository(get(), get()) }
    }
}
