package com.kastapp.fitness.data.local.db.dao

import androidx.room.*
import com.kastapp.fitness.data.model.Lesson

@Dao
abstract class LessonDao : BaseDao<Lesson>() {

    @Query("SELECT * FROM Lesson")
    abstract fun getAll(): List<Lesson>

    @Query("DELETE FROM Lesson")
    abstract fun deleteAll()
}
