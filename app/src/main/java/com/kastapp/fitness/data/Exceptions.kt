package com.kastapp.fitness.data

import java.io.IOException

class NoNetworkException : IOException()
