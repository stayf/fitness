package com.kastapp.fitness.ui.lessons

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.kastapp.fitness.data.model.Lesson
import com.kastapp.fitness.databinding.ItemLessonBinding
import com.kastapp.fitness.util.dp

class LessonsAdapter : RecyclerView.Adapter<ViewHolderItem>() {

    private val lessonList = mutableListOf<Lesson>()

    fun updateLessonList(list: List<Lesson>) {
        lessonList.clear()
        lessonList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderItem {
        return ViewHolderItem(
            ItemLessonBinding.inflate(
                LayoutInflater.from(parent.context)
            )
        )
    }

    override fun getItemCount(): Int {
        return lessonList.size
    }

    override fun onBindViewHolder(holder: ViewHolderItem, position: Int) {
        val lesson = lessonList[position]
        holder.bind(lesson)
    }
}

class ViewHolderItem(
    private val binding: ItemLessonBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(lesson: Lesson) {
        binding.name.text = lesson.name
        binding.description.text = lesson.description
        binding.place.text = lesson.place
        binding.time.text = "${lesson.startTime} - ${lesson.endTime}"
        binding.teacher.text = lesson.teacher

        binding.teacherImage.controller = Fresco.newDraweeControllerBuilder()
            .setOldController(binding.teacherImage.controller)
            .setImageRequest(
                ImageRequestBuilder
                    .newBuilderWithSource(lesson.teacherInfo.imageUrl.toUri())
                    .setResizeOptions(ResizeOptions(140.dp(), 140.dp()))
                    .build()
            ).build()
    }
}