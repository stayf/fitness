package com.kastapp.fitness.data.repository

import com.kastapp.fitness.data.local.db.dao.LessonDao
import com.kastapp.fitness.data.model.Lesson
import com.kastapp.fitness.data.remote.Endpoints
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LessonRepository(
    private val api: Endpoints,
    private val lessonDao: LessonDao
) {
    fun getLessons(isPullRefresh: Boolean = false): Single<List<Lesson>> {
        return Single.create<List<Lesson>> { emitter ->
            if (isPullRefresh) {
                lessonDao.deleteAll()
                emitter.onSuccess(listOf())
            } else {
                emitter.onSuccess(lessonDao.getAll())
            }
        }.subscribeOn(Schedulers.io())
            .flatMap { lessons ->
                if (lessons.isNotEmpty()) {
                    Single.just(lessons)
                } else {
                    api.getLessons().map {
                        lessonDao.upsert(it)
                        it
                    }
                }
            }
    }
}