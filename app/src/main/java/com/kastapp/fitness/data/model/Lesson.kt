package com.kastapp.fitness.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Lesson(
    val name: String,
    val description: String,
    val place: String,
    val teacher: String,
    val startTime: String,
    val endTime: String,
    val weekDay: Int,
    val pay: Boolean,
    val appointment: Boolean,
    val color: String,
    val availability: Int,
    @SerializedName("teacher_v2")
    val teacherInfo: Teacher,
    @SerializedName("appointment_id")
    @PrimaryKey
    val appointmentId: String,
    @SerializedName("service_id")
    val serviceId: String
)


