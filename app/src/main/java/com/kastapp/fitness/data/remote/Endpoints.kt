package com.kastapp.fitness.data.remote

import com.kastapp.fitness.data.model.Lesson
import io.reactivex.Single
import retrofit2.http.*

interface Endpoints {
    @GET("schedule/get_group_lessons_v2/1/")
    fun getLessons(): Single<List<Lesson>>
}
