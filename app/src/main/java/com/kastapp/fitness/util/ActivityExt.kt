package com.kastapp.fitness.util

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import com.kastapp.fitness.R
import com.kastapp.fitness.data.NoNetworkException
import com.kastapp.fitness.ui.common.Event

fun Activity.hideSoftKeyboard() = currentFocus?.apply { hideSoftKeyboard() }

fun Activity.open(intent: Intent, requestCode: Int) {
    checkIntentAndDo(intent) { startActivityForResult(intent, requestCode) }
}

inline fun <reified T : Any> Activity.open(requestCode: Int, bundle: Bundle? = null) {
    val intent = Intent(this, T::class.java)
    checkIntentAndDo(intent) { startActivityForResult(intent, requestCode, bundle) }
}

fun Activity.showSnackBar(error: Event.Error) {
    showSnackBar(getMsgFromError(error))
}

fun Activity.showSnackBarWithRepeat(error: Event.Error, action: View.OnClickListener) {
    val root = findRootLayout()
    root.createSnackBar(
        getMsgFromError(error),
        true,
        getString(R.string.btn_repeat),
        action
    )
}

fun Activity.getMsgFromError(error: Event.Error): String {
    return error.msg ?: error.exception!!.let {
        when (it) {
            is NoNetworkException -> getString(R.string.error_check_network)
            else -> getString(R.string.error_try_later)
        }
    }
}

fun Activity.showSnackBar(@StringRes msg: Int) = findRootLayout().createSnackBar(getString(msg))
fun Activity.showSnackBar(msg: CharSequence) = findRootLayout().createSnackBar(msg)
fun Activity.findRootLayout(): View = findViewById<ViewGroup>(android.R.id.content).getChildAt(0)
