package com.kastapp.fitness.ui.lessons

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.kastapp.fitness.R
import com.kastapp.fitness.databinding.ActivityMainBinding
import com.kastapp.fitness.util.getMsgFromError
import com.kastapp.fitness.util.gone
import com.kastapp.fitness.util.visible
import com.kastapp.fitness.ui.common.EventObserver
import org.koin.androidx.viewmodel.ext.android.viewModel

class LessonsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: LessonsAdapter
    private val lessonsViewModel: LessonsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        adapter = LessonsAdapter()
        binding.list.adapter = adapter

        binding.refresh.setOnRefreshListener { lessonsViewModel.getLessonsEventAsync(true) }
        binding.error.btnRepeat.setOnClickListener { lessonsViewModel.getLessonsEventAsync() }

        lessonsViewModel.getLessonsEvent().observe(
            this, EventObserver({
                adapter.updateLessonList(it)
                binding.error.root.gone()
                binding.refresh.hide()
            }, {
                adapter.updateLessonList(listOf())
                binding.error.root.visible()
                binding.error.msg.text = getMsgFromError(it)
                binding.refresh.hide()
            }, {
                binding.error.root.gone()
                binding.refresh.show()
            })
        )
    }
}
