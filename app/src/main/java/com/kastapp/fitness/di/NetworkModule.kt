package com.kastapp.fitness.di

import android.content.Context
import com.kastapp.fitness.BuildConfig
import com.kastapp.fitness.data.remote.ConnectionInterceptor
import com.kastapp.fitness.data.remote.Endpoints
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkModule {
    val module = module {
        single { createConnectionInterceptor(androidContext()) }
        single { createOkHttpClient(get()) }
        single { createRetrofit(get()) }
        single { createEndpoints(get()) }
    }

    private fun createConnectionInterceptor(context: Context): ConnectionInterceptor {
        return ConnectionInterceptor(context)
    }

    private fun createOkHttpClient(connectionInterceptor: ConnectionInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .addInterceptor(connectionInterceptor)
        return builder.build()
    }

    private fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    private fun createEndpoints(retrofit: Retrofit): Endpoints {
        return retrofit.create(Endpoints::class.java)
    }
}
