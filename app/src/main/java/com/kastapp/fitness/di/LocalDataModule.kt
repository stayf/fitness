package com.kastapp.fitness.di

import androidx.room.Room
import com.kastapp.fitness.BuildConfig
import com.kastapp.fitness.data.local.db.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

object LocalDataModule {
    val module = module {
        single {
            Room.databaseBuilder(
                androidApplication(),
                AppDatabase::class.java,
                BuildConfig.DB_NAME
            ).build()
        }
        single { get<AppDatabase>().lessonDao() }
    }
}
