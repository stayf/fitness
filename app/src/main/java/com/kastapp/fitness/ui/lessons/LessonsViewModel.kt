package com.kastapp.fitness.ui.lessons

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kastapp.fitness.data.model.Lesson
import com.kastapp.fitness.data.repository.LessonRepository
import com.kastapp.fitness.ui.common.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver

class LessonsViewModel(
    private val lessonRepository: LessonRepository
) : ViewModel() {

    private val getLessonsEvent = MutableLiveData<Event<List<Lesson>>>()
    fun getLessonsEvent(): LiveData<Event<List<Lesson>>> = getLessonsEvent

    init {
        getLessonsEventAsync()
    }

    fun getLessonsEventAsync(isPullRefresh: Boolean = false) {
        lessonRepository.getLessons(isPullRefresh)
            .doOnSubscribe { getLessonsEvent.postValue(Event.Loading) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableSingleObserver<List<Lesson>>() {
                override fun onSuccess(result: List<Lesson>) {
                    getLessonsEvent.value = Event.Success(result)
                }

                override fun onError(e: Throwable) {
                    getLessonsEvent.value = Event.Error(e)
                }
            })
    }
}
